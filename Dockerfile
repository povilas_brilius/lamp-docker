FROM opensuse/leap

RUN zypper update -y
RUN zypper in -y apache2 mysql php
RUN zypper in -y php7-xdebug php7-opcache
RUN zypper in -y php7-json composer \
  php7-curl \
  php7-iconv \
  php7-intl \
  php7-dom \
  php7-mbstring \
  php7-xmlreader php7-xmlwriter \
  php7-tokenizer \
  curl \
  apache2-mod_php7

RUN mkdir /srv/www/vhosts

RUN mkdir /var/lib/mysql
RUN chown mysql /var/lib/mysql
RUN mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

RUN zypper in -y unzip git
RUN zypper in -y nodejs npm
RUN zypper in -y php7-mysql
RUN zypper in -y acl

RUN a2enmod vhosts
RUN a2enmod php7
RUN echo "<?php phpinfo();" > /srv/www/htdocs/index.php
RUN chown -Rv :www /srv/www/vhosts
RUN chmod -Rv 2775 /srv/www/vhosts
RUN setfacl -Rdm u::7,g::7,o::5 /srv/www/vhosts

COPY httpd /opt/httpd
COPY mysqld /opt/mysqld
COPY redis /opt/redis
COPY mongodb /opt/mongodb
COPY wrapper.sh /opt/wrapper.sh

RUN a2enmod mod_log_config
RUN a2enmod mod_setenvif
RUN a2enmod rewrite

COPY ci4.starter.conf /etc/apache2/vhosts.d/
COPY slim.app.conf /etc/apache2/vhosts.d/
COPY laminas.app.conf /etc/apache2/vhosts.d/
COPY laravel.app.conf /etc/apache2/vhosts.d/
RUN echo "127.0.0.1 ci4.starter localhost.ci4.starter" >> /etc/hosts
RUN echo "127.0.0.1 slim.starter localhost.slim.starter" >> /etc/hosts
RUN echo "127.0.0.1 laminas.starter localhost.laminas.starter" >> /etc/hosts
RUN echo "127.0.0.1 laravel.app localhost.laravel.app" >> /etc/hosts
RUN touch /etc/my.cnf.d/mysqld_general.cnf
RUN echo "[mysqld]" >> /etc/my.cnf.d/mysqld_general.cnf
RUN echo "general_log=ON" >> /etc/my.cnf.d/mysqld_general.cnf 
RUN echo "general_log_file=/var/log/mysql/mysqld_general" >> /etc/my.cnf.d/mysqld_general.cnf

RUN zypper in -y php-pecl php7-devel
RUN zypper install -y -t pattern devel_C_C++
RUN pecl install redis
RUN echo "extension=redis.so" > /etc/php7/conf.d/redis.ini

RUN pecl install ds
# RUN echo "extension=ds.so" > /etc/php7/conf.d/25-ds.ini

# Laravel
RUN zypper in -y redis

# MongoDB Doctrine
RUN zypper in -y wget
WORKDIR /
RUN rpm --import https://www.mongodb.org/static/pgp/server-4.4.asc
RUN zypper addrepo --gpgcheck "https://repo.mongodb.org/zypper/suse/15/mongodb-org/4.4/x86_64/" mongodb
RUN zypper -n install mongodb-org
RUN mkdir /data/db -pv
RUN pecl install mongodb
RUN echo "extension=mongodb.so" >> /etc/php7/conf.d/mongodb.ini

WORKDIR /srv/www/vhosts

EXPOSE 2121/tcp
EXPOSE 2122/tcp
EXPOSE 2123/tcp
EXPOSE 2124/tcp
EXPOSE 80/tcp

CMD /opt/wrapper.sh
